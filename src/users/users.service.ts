import { ForbiddenException, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/entities/User';
import { QueryFailedError, Repository } from 'typeorm';
import { UpdateUserDto } from './dto/UpdateUser.dto';
import * as bcrypt from 'bcrypt';
import { ConfigService } from '@nestjs/config';
import { Blog } from 'src/entities/blog.entity';
import { UserRole } from 'src/role/enums/UserRole';
import { UserOptionDto } from './dto/UserOption.dto';
import { makeResponse } from 'src/util/output_wrapper';



@Injectable()
export class UsersService {

    constructor(
        @InjectRepository(User) private userRepo: Repository<User>,
        @InjectRepository(Blog) private blogRepo: Repository<Blog>,
        private config: ConfigService,
    ) { }


    async fetchUsers(role: UserRole, userOptionsDto: UserOptionDto) {
        if (role == UserRole.Admin) {
            const users = await this.userRepo.find({
                select: ['id', 'role', 'email', 'firstName', 'lastName'],
                take: userOptionsDto.limit,
                skip: (userOptionsDto.page - 1) * userOptionsDto.limit,
                order: { id: userOptionsDto.orderBy }
            });

            return users;
        }
        return await this.userRepo.find({
            select: ['id', 'role', 'email', 'firstName', 'lastName'],
            where: {
                role: UserRole.Member
            },
            skip: (userOptionsDto.page - 1) * userOptionsDto.limit,
            take: userOptionsDto.limit,
            order: { id: userOptionsDto.orderBy }

        });
    }



    async getUserById(userId: number) {
        try {
            const user = await this.userRepo.findOne({
                select: ['id', 'role', 'email', 'firstName', 'lastName'],
                where: {
                    id: userId
                }
            });

            if (!user) {
                throw new HttpException("Could not find User", HttpStatus.BAD_REQUEST);
            }

            return makeResponse(HttpStatus.OK, "Get User with Id: " + userId, user);

        } catch (error) {
            throw new HttpException("error: " + error.message, HttpStatus.BAD_REQUEST);
        }

    }

    async findOneByUsername(username: string) {
        try {
            const user = await this.userRepo.findOneOrFail({
                select: ['id', 'role', 'email', 'firstName', 'lastName'],
                where: {
                    username: username
                }
            });
            return user;
        } catch (error) {
            throw new HttpException("error: " + error.message, HttpStatus.BAD_REQUEST);
        }

    }


    async adminUpdateUser(role: UserRole, userId: number, userDetails: UpdateUserDto) {
        if (role != UserRole.Admin) {
            throw new ForbiddenException("You don't have the permission to do that")
        }

        const user = this.userRepo.findOne({
            where: {
                id: userId
            }
        })

        if (!user) {
            throw new HttpException(
                'User Not Found',
                HttpStatus.BAD_REQUEST,
            );
        }

        try {
            const hashedPassword = await bcrypt.hashSync(userDetails.password, parseInt(this.config.get<string>("SALT_ROUNDS")));

            userDetails.password = hashedPassword;

            return this.userRepo.update({ id: userId }, { ...userDetails });
        } catch (error) {
            throw new HttpException("error: " + error.message, HttpStatus.BAD_REQUEST);
        }

    }

    async adminDeleteUser(role: UserRole, userId: number) {
        if (role != UserRole.Admin) {
            throw new ForbiddenException("You don't have the permission to do that")
        }


        const user = await this.userRepo.findOne({ where: { id: userId }, relations: ['blogs'] })

        try {
            if (!user) {
                throw new HttpException(
                    'User Not Found',
                    HttpStatus.BAD_REQUEST,
                );
            }

            for (const blog of user.blogs) {
                await this.blogRepo.softDelete(blog.id);
            }

            return this.userRepo.softDelete({ id: userId });

        } catch (error) {
            throw new HttpException("error: " + error.message, HttpStatus.BAD_REQUEST)
        }

    }

    async updateMyUser(req, userDetails: UpdateUserDto) {

        const user = await this.userRepo.findOne({
            where: {
                email: userDetails.email
            }
        })

        if (user) {
            throw new ForbiddenException("email: " + userDetails.email + " is already taken")
        }

        try {
            const hashedPassword = await bcrypt.hashSync(userDetails.password, parseInt(this.config.get<string>("SALT_ROUNDS")));

            userDetails.password = hashedPassword;
            this.userRepo.update({ id: req.userId }, { ...userDetails });
        } catch (error) {
            throw new HttpException("error: " + error.message, HttpStatus.BAD_REQUEST);
        }

    }

    async deleteMyUser(req) {

        try {
            const user = await this.userRepo.findOne({ where: { id: req.user.id }, relations: ['blogs'] })
            for (const blog of user.blogs) {
                await this.blogRepo.softDelete(blog.id);
            }
            this.userRepo.softDelete({ id: req.userId });
        } catch (error) {
            throw new HttpException("error: " + error.message, HttpStatus.BAD_REQUEST)
        }

    }

}