import { Body, Controller, DefaultValuePipe, Delete, Get, HttpStatus, Param, ParseIntPipe, Query, Put, Req, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { UpdateUserDto } from './dto/UpdateUser.dto';
import { AuthGuard } from '@nestjs/passport';
import { makeResponse, makeResponseWithoutData } from 'src/util/output_wrapper';
import { UserOptionDto } from './dto/UserOption.dto';
import { UserRole } from 'src/role/enums/UserRole';




@Controller('users')
export class UsersController {

    constructor(private userService: UsersService) { }


    @ApiTags("User")
    @Get()
    async getUsers(
        @Query() userOptionDto: UserOptionDto, @Req() req
    ) {
        // console.log(userOptionDto)
        if (!req["user"]) {
            return makeResponse(HttpStatus.OK, "Get All Users", await this.userService.fetchUsers(UserRole.Member, userOptionDto));
        }
        return makeResponse(HttpStatus.OK, "Get All Users", await this.userService.fetchUsers(req.user.role, userOptionDto));
    }

    @ApiTags("User")
    @ApiBearerAuth() @UseGuards(AuthGuard('jwt'))
    @Get(':id(\\d+)')
    async getUserById(@Req() req, @Param('id', ParseIntPipe) userId: number) {
        return makeResponse(HttpStatus.OK, "Get User with Id: " + userId, await this.userService.getUserById(userId));
    }

    @ApiTags("User")
    @ApiBearerAuth() @UseGuards(AuthGuard('jwt'))
    @Get(':username')
    async findByUsername(@Param('username') username: string) {
        const regex = /^[a-zA-Z0-9]*[a-zA-Z][a-zA-Z0-9]*$/;
        if (!regex.test(username)) {
            throw new Error('Invalid username');
        }
        return makeResponse(HttpStatus.OK, "Get User with Username: " + username, await this.userService.findOneByUsername(username));
    }

    @ApiTags("User")
    @ApiBearerAuth() @UseGuards(AuthGuard('jwt'))
    @Put("/me")
    async updateThisUser(@Req() req, @Body() updateUserDto: UpdateUserDto) {
        await this.userService.updateMyUser(req, updateUserDto)
        return makeResponseWithoutData(HttpStatus.OK, "Successfully Updated User");

    }

    @ApiTags("User")
    @ApiBearerAuth() @UseGuards(AuthGuard('jwt'))
    @Delete("/me")
    async deleteThisUser(@Req() req) {
        await this.userService.deleteMyUser(req);
        return makeResponseWithoutData(HttpStatus.OK, "Successfully deleted User");

    }


    @ApiTags("User")
    @ApiBearerAuth() @UseGuards(AuthGuard('jwt'))
    @Put(':id(\\d+)')
    async updateUserById(@Req() req, @Param('id', ParseIntPipe) id: number, @Body() updateUserDto: UpdateUserDto) {
        await this.userService.adminUpdateUser(req.user.role, id, updateUserDto);
        return makeResponseWithoutData(HttpStatus.OK, "Successfully Updated User");

    }




    @ApiTags("User")
    @ApiBearerAuth() @UseGuards(AuthGuard('jwt'))
    @Delete(':id(\\d+)')
    async deleteUserById(@Req() req, @Param('id', ParseIntPipe) id: number) {
        await this.userService.adminDeleteUser(req.user.role, id)
        return makeResponseWithoutData(HttpStatus.OK, "Successfully deleted User");
    }

}
