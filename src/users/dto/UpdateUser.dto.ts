import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";
import { UserRole } from "src/role/enums/UserRole";


export class UpdateUserDto{
    @ApiProperty({
        description: "User New Email",
        example: "newJohnD@gmail.com"
    })
    @IsNotEmpty()
    email: string;

    @ApiProperty({
        description: "New User Password",
        example: "newPassword"
    })
    password: string;

    @ApiProperty({ 
        example: UserRole.Member
    })
    role: UserRole;
}