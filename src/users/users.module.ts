import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/entities/User';
import { ConfigModule } from '@nestjs/config';
import { Blog } from 'src/entities/blog.entity';
import { Roles } from 'src/role/decorators/role.decorator';

@Module({
  imports: [TypeOrmModule.forFeature([User, Blog]), ConfigModule.forRoot({})],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService]
})
export class UsersModule { }
