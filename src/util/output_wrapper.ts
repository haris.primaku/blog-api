import { HttpStatus } from "@nestjs/common";

export function makeResponse(statusCode: HttpStatus, message: string, data: any){
    const response = {
      statusCode,
      message,
      data,
    };
    return response;
}

export function makeResponseWithoutData(statusCode: HttpStatus, message: string){
  const response = {
    statusCode,
    message,
  };
  return response;
}
  