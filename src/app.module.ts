import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './entities/User';
import { BlogModule } from './blog/blog.module';
import { AuthModule } from './auth/auth.module';
import { Blog } from './entities/blog.entity';


@Module({
  imports: [ConfigModule.forRoot(), TypeOrmModule.forFeature([User, Blog]), TypeOrmModule.forRootAsync({
    imports: [ConfigModule],
    useFactory: (configService: ConfigService) => ({
      type: 'mysql',
      host: configService.get<string>('DATABASE_HOST'),
      port: parseInt(configService.get<string>('DATABASE_PORT')),
      username: configService.get<string>('DATABASE_USER'),
      password: configService.get<string>('DATABASE_PASS'),
      database: configService.get<string>('DATABASE_NAME'),
      entities: [User, Blog],
      synchronize: false,
    }),
    inject: [ConfigService],
  }), UsersModule, BlogModule, AuthModule],
  controllers: [],
  providers: [],
})
export class AppModule { }