// import { User } from 'src/modules/user/entities/user.entity';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn, DeleteDateColumn } from 'typeorm';
import { User } from './User';

@Entity({ name: "blogs" })
export class Blog {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => User, { onDelete: 'CASCADE' })
  // @JoinColumn({name:"userId",referencedColumnName:"id"})
  user: User;

  @Column()
  userId: number

  @Column()
  title: string;

  @Column()
  content: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  createdDate: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}