import { Module, forwardRef } from '@nestjs/common';
import { BlogService } from './blog.service';
import { BlogController } from './blog.controller';
import { UsersModule } from 'src/users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BlogRepository } from './blog.repository';
import { PassportModule } from '@nestjs/passport';
import { Blog } from 'src/entities/blog.entity';
import { User } from 'src/entities/User';
import { Roles } from 'src/role/decorators/role.decorator';

@Module({
  controllers: [BlogController],
  providers: [BlogService, BlogRepository/*RolesGuard, {
    provide: APP_GUARD,
    useClass: RolesGuard,
  },*/],
  imports: [UsersModule, TypeOrmModule.forFeature([Blog, User]), PassportModule],
  exports: [BlogService, BlogRepository]
})
export class BlogModule {}
