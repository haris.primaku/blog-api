import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class PaginationBlogOptionDto {
  @ApiProperty({
    description: "Page Number",
    example: 1
  })
  @IsNotEmpty()
  page?: number = 1;

  @ApiProperty({
    description: "Number of Blogs to Get",
    example: 5
  })
  limit?: number = 5;
}
