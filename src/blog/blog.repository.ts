import { Injectable} from '@nestjs/common';
import { BlogRequestDto} from './dto/blog-request.dto';

import { InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { Blog } from 'src/entities/blog.entity';

@Injectable()
export class BlogRepository {
    @InjectRepository(Blog)
    private blogsRepository: Repository<Blog>;

    constructor(db: DataSource) {
        this.blogsRepository = db.getRepository(Blog);
    }

    delete(id: number) {
        return this.blogsRepository.softDelete(id);
    }

    findAll(skip: number, limit: number) {
        return this.blogsRepository.find({
            skip: skip,
            take: limit,
            order: {
                createdDate: "DESC"
            }
        });
    }

    create(userId: number, title: string, content: string) {
        const blog = this.blogsRepository.create({
            userId: userId,
            title: title,
            content: content,
            createdDate: new Date()
        });
        return this.blogsRepository.save(blog);
    }

    update(id: number, blogRequestDto: BlogRequestDto) {
        const result = this.blogsRepository.update({ id }, {
                        ...blogRequestDto,
                        createdDate: new Date(),
                        });
        return result;
    }

    async getAllBlogsByUser(id: number, skip: number, limit: number) {
        // const blogs = await this.blogsRepository
        // .createQueryBuilder('blog')
        // .leftJoinAndSelect('blog.user', 'user')
        // .where('blog.user = :id', { id })
        // .getMany();
        

        const blogs2 = await this.blogsRepository.find({
            where: {
              userId: id,
            },
            skip: skip,
            take: limit,
            order: {
                createdDate: "DESC"
            }
        });
        console.log(blogs2);

        return blogs2;
    }

    getBlogById(id: number) {
        return this.blogsRepository.findOne({
            where: {
                id: id
            }
        });
    }
}
