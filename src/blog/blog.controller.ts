import { Controller, Get, Post, Body, Patch, Param, Delete, BadRequestException, Req, UseGuards, UnauthorizedException, ParseIntPipe, HttpStatus, Query } from '@nestjs/common';
import { BlogService } from './blog.service';
import { BlogRequestDto } from './dto/blog-request.dto';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { RolesGuard } from '../role/guards/roles.guard';
import { UserRole } from 'src/role/enums/UserRole';
import { makeResponse, makeResponseWithoutData } from 'src/util/output_wrapper';
import { PaginationBlogOptionDto } from './dto/PaginationBlogOption.dto';

@Controller('blog')
@ApiTags('Blog')
export class BlogController {
  constructor(private readonly blogService: BlogService) {}

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))                                     
  @Post()
  async create(@Req() req, @Body() blogRequestDto: BlogRequestDto) {
    const userId = req.user.id;  
    if (!blogRequestDto.title || !blogRequestDto.content) {
      throw new BadRequestException('Missing required properties');
    }
    const createdBlog = await this.blogService.create(userId, blogRequestDto);
    return makeResponse(HttpStatus.OK, "Successsfully created Blog", createdBlog);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get('me')
  async getAllBlogsByMe(@Req() req, @Query() paginationBlogOptionDto: PaginationBlogOptionDto) {
    const limit = paginationBlogOptionDto.limit;
    const page = paginationBlogOptionDto.page;
    if(limit < 1 || page < 1){
      throw new BadRequestException('Invalid pagination parameters');
    }
    const skip = (page - 1) * limit;
    const userId = req.user.id;
    const myBlogs = await this.blogService.getAllBlogsByUser(userId, skip, limit);
    return makeResponse(HttpStatus.OK, "Successfully Get 'My' Blogs", myBlogs);
  }

  @Get(':id(\\d+)')
  async findOne(@Param('id', ParseIntPipe) id: number) {
    const theFindBlog = await this.blogService.findOne(+id);
    return makeResponse(HttpStatus.OK, "Successfully Get Blog by Id", theFindBlog);
  }

  @Get(':username')
  async findBlogByUsername(@Param('username') username: string, @Query() paginationBlogOptionDto: PaginationBlogOptionDto) {
    const limit = paginationBlogOptionDto.limit;
    const page = paginationBlogOptionDto.page
    if(limit < 1 || page < 1){
      throw new BadRequestException('Invalid pagination parameters');
    }
    const skip = (page - 1) * limit;
    const regex = /^[a-zA-Z0-9]*[a-zA-Z][a-zA-Z0-9]*$/;
    if (!regex.test(username)) {
      throw new Error('Invalid username');
    }
    const theFindBlog = await this.blogService.findOneUsername(username, skip, limit);
    return makeResponse(HttpStatus.OK, "Successfully Get Blog by Username", theFindBlog);
  }

  @Get()
  async findAll(@Query() paginationBlogOptionDto: PaginationBlogOptionDto) {
    const limit = paginationBlogOptionDto.limit;
    const page = paginationBlogOptionDto.page;
    if(limit < 1 || page < 1){
      throw new BadRequestException('Invalid pagination parameters');
    }
    const skip = (page - 1) * limit;
    const allBlogs = await this.blogService.findAll(skip, limit);
    return makeResponse(HttpStatus.OK, "Successfully Get All Blogs", allBlogs);
  }


  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Patch(':id(\\d+)')
  async update(@Req() req, @Param('id', ParseIntPipe) id: number, @Body() blogRequestDto: BlogRequestDto) {
    const userId = req.user.id;

    const isAdmin = req.user.role == UserRole.Admin;

    const updatedBlog = await this.blogService.update(isAdmin, +id, userId, blogRequestDto);
    return makeResponse(HttpStatus.OK, "Successfully Update Blog", updatedBlog);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'), RolesGuard)
  @Delete(':id(\\d+)')
  async remove(@Req() req, @Param('id', ParseIntPipe) id: number) {
    const userId = req.user.id;

    const isAdmin = req.user.role == UserRole.Admin;
    const messageDelete = await this.blogService.remove(isAdmin, +id, userId);
    return makeResponseWithoutData(HttpStatus.OK, messageDelete);
  }
}
