import {Inject, Injectable, NotFoundException, UnauthorizedException, forwardRef} from '@nestjs/common';
import { BlogRequestDto} from './dto/blog-request.dto';
import { UsersService } from 'src/users/users.service';
import { BlogRepository } from './blog.repository';

@Injectable()
export class BlogService {
  
  constructor(
    private blogsRepository: BlogRepository,
    private readonly userService: UsersService
  ) {}
  
  async create(userId: number, blogRequestDto: BlogRequestDto) {
    try {
      const user = await this.userService.getUserById(userId);
      return this.blogsRepository.create(userId, blogRequestDto.title, blogRequestDto.content);
    } catch (error) {
      throw error;
    }
  }

  async findAll(skip: number, limit: number){
    return this.blogsRepository.findAll(skip, limit);
  }

  async findOne(id: number) {
    const result = await this.blogsRepository.getBlogById(id);
    if (!result) {
      throw new NotFoundException('Blog not found');
    }
    return result;
  }

  async remove(isAdmin: boolean,id: number, userId: number){
    
    const blog = await this.findOne(id);
    if (!blog) {
      throw new NotFoundException('Blog not found');
    }
    // console.log(userId);
    // console.log(blog.userId);
    if (blog.userId !== userId && !isAdmin) {
      throw new UnauthorizedException("You don't have permission to delete this blog");
    }

    const result = await this.blogsRepository.delete(id);
    if(result.affected == 0) {
      throw new NotFoundException('Blog not found');
    }
    else {
      return "Deleted Blog with id " + id.toString();
    }
    
  }

  async update(isAdmin: boolean, id: number, userId: number, blogRequestDto: BlogRequestDto) {

    const blog = await this.findOne(id);
    if (!blog) {
      throw new NotFoundException('Blog not found');
    }

    if (blog.userId !== userId && !isAdmin) {
      throw new UnauthorizedException("You don't have permission to update this blog");
    }

    const result = await this.blogsRepository.update(id, blogRequestDto);
    if(result.affected == 0) {
      throw new NotFoundException('Blog not found');
    }
    else {
      return this.findOne(id);
    }
  }

  // async removeByUserId(id: number) {
  //   const blogs = await this.blogsRepository.getAllBlogsByUser(id, skip, limit);
  //     for (const blog of blogs) {
  //       await this.blogsRepository.delete(blog.id);
  //     }
  // }

  async getAllBlogsByUser(id: number, skip: number, limit: number) {
    return await this.blogsRepository.getAllBlogsByUser(id, skip, limit);
  }

  async findOneUsername(username: string, skip: number, limit: number) {
    try {
      const user = await this.userService.findOneByUsername(username);
      return await this.blogsRepository.getAllBlogsByUser(user.id, skip, limit);
    } catch (error) {
      throw error;
    }
  }

}
