import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtStrategy } from './strategy/jwt.strategy';
import { ConfigModule } from '@nestjs/config';
import { User } from 'src/entities/User';
import { Blog } from 'src/entities/blog.entity';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { Roles } from 'src/role/decorators/role.decorator';

@Module({
  imports: [TypeOrmModule.forFeature([User, Blog]), JwtModule.register({}), ConfigModule.forRoot({})],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy]
}) 
export class AuthModule {}
