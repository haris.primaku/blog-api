import { ApiProperty } from "@nestjs/swagger";
import { IsAlphanumeric, IsEmail, IsNotEmpty } from "class-validator";
import { UserRole } from "src/role/enums/UserRole";

export class CreateUserDto {

  @ApiProperty({ description: "User First Name", example: "John" })
  @IsNotEmpty()
  firstName: string;

  @ApiProperty({ description: "User Last Name", example: "Doe" })
  @IsNotEmpty()
  lastName: string;

  @ApiProperty({ description: "Username", example: "JohnDUser" })
  @IsNotEmpty()
  @IsAlphanumeric()
  username: string;

  @ApiProperty({ description: "User Email", example: "jdoe.test@gmail.com" })
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty({ description: "User Password", example: "johnDoePassword123" })
  @IsNotEmpty()
  password: string;

  @ApiProperty({ description: "User Role", example: "admin"})
  @IsNotEmpty()
  role: UserRole;
}