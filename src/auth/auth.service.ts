import { ForbiddenException, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/entities/User';
import { QueryFailedError, Repository } from 'typeorm';
import { CreateUserDto } from './dto/CreateUser.dto';
import { UserLoginDto } from './dto/UserLogin.dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(User) private userRepo: Repository<User>,
        private config: ConfigService,
        private jwt: JwtService,
    ) { }

    validateUsername(str) {
        const regex = /[a-zA-Z]/;
        return regex.test(str);
    }

    validateEmail(str) {
        const regexExp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/gi;
        return regexExp.test(str);
    }


    async createUser(userDetails: CreateUserDto) {
        // console.log(userDetails)

        let check = await this.userRepo.findOne({
            where: {
                email: userDetails.email
            }
        })

        if (check) {
            throw new ForbiddenException("Email is already taken")
        }

        check = await this.userRepo.findOne({
            where: {
                username: userDetails.username
            }
        })
        if (check) {
            throw new ForbiddenException("Username is already taken")
        }


        if (!this.validateUsername(userDetails.username)) {
            throw new HttpException("Username must have atleast one letter", HttpStatus.BAD_REQUEST);
        }

        try {
            const hashed_pass = await bcrypt.hashSync(userDetails.password, parseInt(this.config.get<string>("SALT_ROUNDS")));

            const newUser = this.userRepo.create({
                firstName: userDetails.firstName,
                lastName: userDetails.lastName,
                email: userDetails.email,
                username: userDetails.username,
                role: userDetails.role,
                password: hashed_pass
            });
            await this.userRepo.save(newUser);
            return this.signToken(newUser.id, newUser.email);
        }
        catch (error) {
            throw new HttpException("Error: " + error.message, HttpStatus.BAD_REQUEST)

        }
    }

    async userLogin(userDetails: UserLoginDto) {
        let user: User;
        if (this.validateEmail(userDetails.loginVal)) {
            // console.log("login via email")
            user = await this.userRepo.findOne({
                where: {
                    email: userDetails.loginVal,
                },
            });
            if (!user) {
                throw new HttpException('Email not found', HttpStatus.BAD_REQUEST);
            }
        } else {
            // console.log("login via username")
            user = await this.userRepo.findOne({
                where: {
                    username: userDetails.loginVal,
                },
            });
            if (!user) {
                throw new HttpException('Username not found', HttpStatus.BAD_REQUEST);
            }
        }

  

        const passwordsMatch = await bcrypt.compareSync(
            userDetails.password,
            user.password,
        );

        if (!passwordsMatch) {
            throw new ForbiddenException('Credentials incorrect');
        }

        return this.signToken(user.id, user.email);
    }

    async signToken(userId: number, email: string): Promise<{ access_token: string }> {
        const payload = {
            sub: userId,
            email,
        };
        const secret = this.config.get('JWT_SECRET');

        const token = await this.jwt.signAsync(
            payload,
            {
                expiresIn: '15m',
                secret: secret,
            },
        );

        return {
            access_token: token,
        };
    }
}
